from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_debug import Debug
from flask_cors import CORS
from flask_migrate import Migrate
from flask_wtf.csrf import CSRFProtect
from flask_bootstrap import Bootstrap5
from flask_login import LoginManager
from flask_authorize import Authorize

db = SQLAlchemy()
api = Api()
dbug = Debug()
cors = CORS()
migrate = Migrate()
csrf = CSRFProtect()
bootstrap = Bootstrap5()
login = LoginManager()
authorize = Authorize()