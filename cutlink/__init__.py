from flask import Flask
from .config import config
from .extentions import db,dbug,cors,migrate,api,csrf,bootstrap,authorize,login
from .models import User

def create_app(config_name='default'):
    app = Flask(__name__)  
    app.config.from_object(config[config_name])
    
    db.init_app(app)
    dbug.init_app(app)
    cors.init_app(app)
    csrf.init_app(app)
    bootstrap.init_app(app)
    login.init_app(app)
    authorize.init_app(app)



    migrate.init_app(app,db)

    from .auth.routes import main as auth_bp
    from .main.routes import main as app_bp
    # from .apis import init_resourcess

    # init_resourcess()
    # api.init_app(app)
    app.register_blueprint(app_bp)
    app.register_blueprint(auth_bp,url_prefix='/auth')



    
    return app

    