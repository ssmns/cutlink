from flask import Blueprint,render_template,request,current_app,send_from_directory,redirect
import os
from cutlink.extentions import db,login,authorize
from cutlink.models import Link,User
from cutlink.utils.generate import generate_code
from datetime import datetime
from cutlink.utils.time import add_time_from_now
from .forms import GenerateShortLink
from flask import jsonify
import flask_login

time_expire = {'1':10,'2':20,'3':24*60,'4':24*60*7}

main = Blueprint('main',__name__)

@main.route("/",methods=['GET','POST'])
def index():
    form = GenerateShortLink()
    number_of_active_user=len(User.query.all())
    number_of_active_link=len(Link.query.all())
    context = {"generate":True,"links":number_of_active_link,"users":number_of_active_user}
    if form.validate_on_submit():
        target = form.link.data
        clicked = form.expire.data
        expire_time = time_expire[form.expire_at.data]
        if not flask_login.current_user:
            print('not login')  
            return render_template('index.html',form=form,context=context)     
        while True:
            code = generate_code(8)
            search = Link.query.filter_by(code=code).first()
            if not search:
                clicked = clicked if clicked and clicked > 0 else -1
                link = Link(code=code,target=target,max_clicked=clicked,expire_at=add_time_from_now(expire_time))
                db.session.add(link)    
                db.session.commit()
                context = {"target":target,"code":code,"expire_at":link.expire_at,"generate":False,}             
                break
    return render_template('index.html',form=form,context=context)
        
@main.route("/s/<code_number>")
def redirect_to_tareget(code_number):
    search = Link.query.filter_by(code=code_number).first()
    if (search.expire_at < datetime.utcnow()) or search.max_clicked == search.counter:
        db.session.delete(search)
        db.session.commit()
        return "Expire link or maximum clicked is reached",404

    if search:
        search.counter += 1
        db.session.commit()
        return render_template("redirect.html",target=search.target)


@main.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404

@main.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500