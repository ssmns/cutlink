from flask_wtf import FlaskForm
from wtforms import StringField,SubmitField,IntegerField,SelectField
from wtforms.validators import DataRequired,url,length


class GenerateShortLink(FlaskForm):
    link = StringField('Target Link (http[s]):',validators=[DataRequired(),url(),length(max=1024)],)
    expire = IntegerField('Number of use link to Expire:',default=-1)
    expire_at = SelectField('Expile age of link:',choices=((("1",'10 minutes'),("2",'30 minutes'),("3",'1 day'),("4",'1 week'))),default="1")

    submit = SubmitField('generate')
