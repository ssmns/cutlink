import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'A_Strong_Secret_Key'

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    FLASK_DEBUG_DISABLE_STRICT = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    ENV = 'development'
    FLASK_ENV = 'development'
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URI') or \
        'sqlite:///' + 'database.db'


class TestingConfig(Config):
    ENV = 'testing'
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URI') or \
        'sqlite:///'


class ProductionConfig(Config):
    ENV = 'production'
    PASSWORD=os.environ.get('DB_PASSWORD')
    USERNAME=os.environ.get('DB_USERNAME')
    PORT=os.environ.get('DB_PORT')
    URL=os.environ.get('DB_URL')
    DB_NAME=os.environ.get('DB_NAME')
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or \
        f"postgresql://{USERNAME}:{PASSWORD}@{URL}:{PORT}/{DB_NAME}"


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
