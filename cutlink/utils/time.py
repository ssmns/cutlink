from datetime import datetime,timedelta

def add_time_from_now(minutes=10):
    return datetime.utcnow() + timedelta(minutes=minutes)