
from cutlink.extentions import db
from flask_authorize import RestrictionsMixin, AllowancesMixin
from werkzeug.security import generate_password_hash,check_password_hash
from flask_login import UserMixin

# mapping tables
UserGroup = db.Table(
    'user_group', db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('group_id', db.Integer, db.ForeignKey('groups.id'))
)


UserRole = db.Table(
    'user_role', db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('role_id', db.Integer, db.ForeignKey('roles.id'))
)



class User(db.Model,UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password_hash = db.Column(db.String(255), nullable=False)

    # `roles` and `groups` are reserved words that *must* be defined
    # on the `User` model to use group- or role-based authorization.
    roles = db.relationship('Role', secondary=UserRole)
    groups = db.relationship('Group', secondary=UserGroup)


    @property
    def password(self):
        raise AttributeError('Password is not readable attribute')

    @password.setter
    def password(self,password):
        self.password_hash = generate_password_hash(password)
    
    def verify_password(self,password):
        return check_password_hash(self.password_hash,password)

from cutlink.extentions import login
@login.user_loader
def load_user(user_id):
    return User.query.get(user_id)

class UserQuery(object):
    @staticmethod
    def get_user_id_by_email_or_username(email=None, username=None):
        user = User.query.filter((User.email == email) | (User.username == username)).first()
        return user.id if hasattr(user, 'id') else None  
    
    @staticmethod
    def get_user(username_email=None):
        user = User.query.filter((User.email == username_email) | (User.username == username_email)).first()
        return user.id if hasattr(user, 'id') else None  



class Group(db.Model, RestrictionsMixin):
    __tablename__ = 'groups'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False, unique=True)

class Role(db.Model, AllowancesMixin):
    __tablename__ = 'roles'
    __allowances__ = '*'
    __restrictions__ = {}

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False, unique=True)


