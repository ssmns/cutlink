from datetime import datetime
from cutlink.extentions import db 
from cutlink.utils.time import add_time_from_now
from flask_authorize import PermissionsMixin

from .auth import User




class Link(db.Model,PermissionsMixin):
    __tablename__= "links"
    __permissions__ = dict(
        owner=['read', 'update', 'delete', 'revoke'],
        group=['read', 'update'],
        other=['read']
    )

    id = db.Column(db.Integer()  , primary_key=True,unique=True)

    code = db.Column(db.String(8),    nullable=False , unique=True)
    target = db.Column(db.String(1024), nullable=False , unique=False)

    created_at = db.Column(db.DateTime, nullable=True, default=datetime.utcnow)
    expire_at = db.Column(db.DateTime, nullable=True, default=add_time_from_now)

    counter = db.Column(db.Integer()  , default=0)
    max_clicked = db.Column(db.Integer()  , default=-1)

    def __repr__(self):
        return f"Link('{self.code}', '{self.expire_at}', '{self.counter}')"