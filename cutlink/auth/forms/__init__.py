from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError


class LoginForm(FlaskForm):
    username_email = StringField('Username/Email', validators=[DataRequired(), ])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')

    def validate_username_email(self, username_email):
        # Import them inside the methods that are using the Model
        from cutlink.models.auth import UserQuery
        user = UserQuery.get_user(username_email=username_email.data)
        if not user:
            raise ValidationError('That username/email is not exist. Please choose a different one.')

    def validate_password(self, username_email):
        # Import them inside the methods that are using the Model
        from cutlink.models.auth import UserQuery
        user = UserQuery.get_user(username_email=username_email.data)
        if user and not user.verify_password(self.password.data):
            raise ValidationError('That password is not valid.')


class RegistrationForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')

    def validate_username(self, username):
        # Import them inside the methods that are using the Model
        from cutlink.models.auth import UserQuery
        user = UserQuery.get_user_id_by_email_or_username(username=username.data)
        if user:
            raise ValidationError('That username is taken. Please choose a different one.')

    def validate_email(self, email):
        # Import them inside the methods that are using the Model
        from cutlink.models.auth import UserQuery
        user = UserQuery.get_user_id_by_email_or_username(email=email.data)
        if user:
            raise ValidationError('That email is taken. Please choose a different one.')