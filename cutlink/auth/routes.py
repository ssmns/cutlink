from flask import Blueprint,render_template,request,url_for,redirect


main = Blueprint('auth',__name__)

from .forms import RegistrationForm
from .forms import LoginForm
from cutlink.models.auth import User , UserQuery
from cutlink.extentions import db
from flask_login import login_required,login_user,logout_user

@main.route("/register",methods=['GET','POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data,password=form.password.data,email=form.email.data)
        db.session.add(user)
        db.session.commit()    
        return ('ok')
    return render_template('auth/signup.html',form=form)


@main.route("/login",methods=['GET','POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():    
        _id = UserQuery.get_user(form.username_email.data)
        user = db.session.query(User).filter_by(id=_id).first()
        if user and user.verify_password(form.password.data):
            login_user(user)
            next = request.args.get('next')
            if next is None or next.startswith('/'):
                next = url_for('main.index')
            return redirect(next)
        return redirect(url_for('main.index'))
     
    return render_template('auth/signin.html',form=form)


@main.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))

@main.route("/secret")
@login_required
def test():
    return "ok"