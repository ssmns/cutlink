import os
from flask_migrate import Migrate
from dotenv import load_dotenv



dotenv_path = os.path.join(os.path.dirname(__file__),'.flaskenv')
if os.path.exists(dotenv_path):
    denv = load_dotenv(dotenv_path)

from cutlink import create_app, db
app = create_app(os.getenv('FLASK_CONFIG') or 'default')


@app.shell_context_processor
def make_shell_context():
    return dict(db=db,)
